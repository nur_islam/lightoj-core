import cookies from 'js-cookie'


const tokenCookieName = 'token'

export const state = () => ({
  loggedIn: false,
  userData: {},
  jwtToken: null
})

export const mutations = {
  INIT_LOGIN(state, user){
    state.loggedIn = true;
    state.userData = user;
  },
  LOGGED_IN (state, user){
    state.loggedIn = true;
    state.userData = user.userData;
    state.jwtToken = user.token;
  },
  LOGGED_OUT (state){
    state.loggedIn = false;
    state.userData = {};
    state.jwtToken = null;
  }
}

export const actions = {
  logout({commit}){
    commit('LOGGED_OUT');
    cookies.remove(tokenCookieName);
  },
  async login({commit}, data){
    /*
      data should contain two fields:
      handle, password
    */
    let returnData = {
      'success': true,
      'errors': []
    }
    try{
      const resp = await this.$axios.$post('auth/login', {
        handle: data.handle,
        password: data.password
      });
      commit('LOGGED_IN', resp);
      cookies.set(tokenCookieName, resp.token);
    }catch(e){
        returnData.success = false;
        if(e.response.status == 422){
            /* its a validation error */
            returnData.errors = e.response.data.errors
        }else{
            returnData.errors = ["Something weird! Please try again."]
        }
    }
    return returnData
  },
  async register({commit}, data){
    /*
      data should contain four fields:
      fullname, handle, email, password
    */
    let returnData = {
      'success': true,
      'errors': []
    }
    try{
        const resp = await this.$axios.$post('auth/register', data)
        this.$router.push('/auth/login')
    }catch(e){
        console.log(e)
        returnData.success = false;
        if(e.response.status == 422){
            /* its a validation error */
          returnData.errors = e.response.data.errors
        }else{
          returnData.errors = ["Something weird! Please try again."]
        }
    }
    return returnData;
  }
}