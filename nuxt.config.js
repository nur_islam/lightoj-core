const config = require('config');

module.exports = {
  /*
  ** Headers of the page
  */
  head: {
    title: 'LightOJ',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Nuxt.js project' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css?family=Poppins:200,300,400,500,600' },
      { rel: 'stylesheet', href: 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css' },
    ],
  },
  /*
  ** Global CSS
  */
  css: [
    '~/assets/vendors/material-icons/material-icons.css',
    '~/assets/vendors/mono-social-icons/monosocialiconsfont.css',
    '~/assets/vendors/feather-icons/feather.css',
    '~/assets/scss/style.scss'
  ],
  /*
  ** Add axios globally
  */
  modules: [
    '@nuxtjs/axios',
    ['bootstrap-vue/nuxt', { css: false }]
  ],
  build: {
    vendor: [],
    /*
    ** Run ESLINT on save
    */
    extend (config, ctx) {
      config.node = {
        console: true,
        fs: 'empty',
        net:'empty',
        tls:'empty',
      }
      /*
      if (ctx.isDev && ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
      */
    }
  },
  axios: {
    baseURL: config.nuxt.apiBaseUrl
  },
  router: {
    middleware: 'i18n'
  },
  plugins: [
    {src: '~plugins/i18n'}
  ]
}
