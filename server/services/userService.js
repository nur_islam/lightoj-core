import bcrypt from 'bcrypt';

import User from '../models/user';
import { create } from 'domain';


const getHashedPassword = async (password) =>{
  const salt = await bcrypt.genSalt(10);
  const hash = await bcrypt.hash(password, salt);
  return hash
}

const matchPasswordWithHash = async (givenPassword, hashedPassword) =>{
  const matches = await bcrypt.compare(givenPassword, hashedPassword)
  if(matches) return true;
  return false;
}


export async function createUser(userData) {
  const createData = {
    userNameStr: userData.fullname,
    userHandleStr: userData.handle,
    userEmailStr: userData.email,
    userPassStr: await getHashedPassword(userData.password)
  }
  console.log(createData)
  return new User(createData).save();
}

export async function checkEmailExist(email){
  const userCount = await User.where('userEmailStr', email).count('userId');
  if(userCount > 0){
    return true
  }
  return false
}

export async function checkHandleExist(handle){
  const userCount = await User.where('userHandleStr', handle).count('userId');
  if(userCount > 0){
    return true
  }
  return false
}

export async function loginUserWithHandle(handle, password){
  const user = await User.where('userHandleStr', handle).fetch({'required': true})
  if(!user) return null // No user found with the given handle
  const match = await matchPasswordWithHash(password, user.get('userPassStr'))
  if(match === true){
    return user;
  }
  return null
}