import jwt from 'jsonwebtoken'
import config from 'config'

export function jwtSign(data){
  const jwtData =  jwt.sign(data, config.jwt.secret, {expiresIn: config.jwt.expiresIn})
  return jwtData;
}