const usersData = [
  { 
    userId: 1,
    userNameStr: 'Test User', 
    userHandleStr: 'testuser', 
    userEmailStr: 'test@test.com',
    // password is test123
    userPassStr: '$2b$10$vy3Nk6eO8X0k9CJIgvhgxul4XDymKz74htnyr5.POiGI1zL4qnnAi',
    created_at: new Date(),
    updated_at: new Date()
  }
]

exports.seed = function(knex, Promise) {
  // Deletes ALL existing entries
  return knex('users').del()
    .then(function () {
      // Inserts seed entries
      return knex('users').insert(usersData);
    });
};
