import { Router } from 'express'
import { check, validationResult } from 'express-validator/check'

import * as userService from '../services/userService'
import { jwtSign } from '../utils';

const router = Router()

router.post('/register', [
    check('email').exists().withMessage('Email field can\'t be empty')
      .isEmail().withMessage('Please provide a valid email address')
      .custom(async (value, {req, loc, path}) => {
        const exist = await userService.checkEmailExist(value)
        if(exist === true) return false
        return value
      }).withMessage('Email already exists'),
    check('handle').exists().withMessage('Handle field can\'t be empty')
      .isLength({ min: 3 }).withMessage('Handle should have at least 3 characters')
      .custom(async (value, {req, loc, path}) => {
        const exist = await userService.checkHandleExist(value)
        if(exist === true) return false
        return value
      }).withMessage('Handle already exists')
  ],
  async function (req, res, next) {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array().map(error => error.msg) });
    }
    try{
      const user = await userService.createUser(req.body)
      return res.status(200).json({'success': true, 'msg': 'Successfully created account. Please verify the account'})
    }catch(err){
      next(err)
    }
})


router.post('/login', [
  check('handle').exists().withMessage('Please provide your handle'),
  check('password').exists().withMessage('Please provide your password')
], async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(422).json({ errors: errors.array().map(error => error.msg) });
  }
  const user = await userService.loginUserWithHandle(req.body.handle, req.body.password)
  if(!user){
    return res.status(422).json({ errors: ['Handle and password combination didnn\'t match']})
  }
  console.log(user)
  const userData = {
    fullName: user.get('userNameStr'),
    handle: user.get('userHandleStr'),
    email: user.get('userEmailStr')
  }
  const returned_data = {
    'msg': 'Successfully logged in',
    userData,
    token: jwtSign(userData)
  }

  return res.status(200).json(returned_data)
})

export default router